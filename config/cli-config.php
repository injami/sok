<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use App\Factory\EntityManagerFactory;
use Dotenv\Dotenv;

require __DIR__ . '/../vendor/autoload.php';

$env = Dotenv::createImmutable(dirname(__DIR__));
$env->load();

$emFactory = new EntityManagerFactory();
$entityManager = $emFactory->createEntityManager();

return ConsoleRunner::createHelperSet($entityManager);
