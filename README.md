# SOK

## Installation

* Install [Composer](https://getcomposer.org/download)
* Install dependencies: `composer install`
* Setup database
* Add following lines to the .env file and fill them with database information

```
DB_NAME=
DB_USER=
DB_PASSWORD=
DB_HOST=
DB_DATABASE_DRIVER=
```

Supported databases [docs](https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html)

Additionally key APP_ENV have one value - development. Anything else will switch mode to production

* Create migrations from root of the project.

```bash
$ php ./vendor/bin/doctrine-migrations diff
```

* Run migrations from root of the project.

```bash
$ php ./vendor/bin/doctrine-migrations migrate
```

* And feel free to use the power of doctrine CLI.

```bash
$ php ./vendor/bin/doctrine --help
```

* /templates/templates_cache & system /tmp directories must have reade/write permissions for server!!!


## Usage 

Project is ready! You can run it with built-in php server

```bash
$ cd /project_root/public
$ php -S localhost:8000
```

Or use Apache/nginx/something
