<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ClassMetadata;
use App\Entity\Repository;

/**
 * @ORM\Entity(repositoryClass="App\Entity\Repository\UsersRepository")
 * @ORM\Table(name="Users")
 */

class Users
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */

    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */

    private string $password;

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setPassword(string $password) : void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
   }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
