<?php

namespace App\Controllers;

use App\Factory\EntityManagerFactory;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

class Controller
{

    protected FilesystemLoader $loader;
    protected Environment $twig;

    public function __construct()
    {
        $this->loader = new FilesystemLoader(TEMPLATES);
        $this->twig = new Environment($this->loader, ['cache' => TEMPLATES_CACHE, 'auto_reload' => true]);
    }

    protected function loadTwigTemplate(string $name, array $vars = [])
    {
        $template = $this->twig->load($name);
        echo $template->render($vars);
    }

    protected function validate(array $array)
    {
        $errors = [];

        foreach ($array as $key => $item) {
            if (empty($item)) {
                array_push($errors, $key . ' is required');
            }
        }
        return $errors;
    }

    public function checkLogin()
    {

        if (isset($_SESSION['user'])) {
            return true;
        }

        return false;
    }

    protected function getEntityManager()
    {
        $factory = new EntityManagerFactory();
        return $factory->createEntityManager();
    }
}
