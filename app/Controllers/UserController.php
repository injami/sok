<?php


namespace App\Controllers;


use App\Entity\Users;
use App\Entity\Repository\UsersRepository;
use Doctrine\ORM\ORMException;

class UserController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function register()
    {
        $user = new Users();

        $input = input()->all();

        $errors = $this->validate($input);

        if (count($errors) > 0) {
            echo $errors;
            die();
        }

        $user->setPassword(password_hash($input['password'], PASSWORD_BCRYPT));
        $user->setName($input['name']);

        $em = $this->getEntityManager();

        try {
            $em->persist($user);
            $em->flush();
        } catch (ORMException $e) {
        }

        return response()->redirect('/cabinet');
    }

    public function authorize()
    {
        $input = input()->all();
        try {
            $user = $this->getEntityManager()->getRepository(Users::class)->findOneBy(array('name' => $input['name']));

        } catch (ORMException $e) {
        }


        if (isset($user)) {
            if (password_verify($input['password'], $user->getPassword())) {
                $_SESSION['user'] = $user->getId();
                return response()->redirect('/cabinet');
            }
            else {
                return response()->redirect('/error');
            }
        }
        else {
            return response()->redirect('/error');
        }
    }

    public function logout()
    {
        if(isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }

        return response()->redirect('/');
    }
}
