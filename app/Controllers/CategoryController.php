<?php


namespace App\Controllers;


use App\Entity\Category;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\Mapping\MappingException;

class CategoryController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function cabinet()
    {
        if (!$this->checkLogin()) response()->redirect('/');

        $em = $this->getEntityManager();

        $repo = $em->getRepository(Category::class);

        $options = array(
            'decorate' => true,
            'rootOpen' => '<ul>',
            'rootClose' => '</ul>',
            'childOpen' => '<li>',
            'childClose' => '</li>',
            'nodeDecorator' => function($node) {
                return '<div class="category-container" id="category'. $node['id'] .'">
    <div class="category-head">
        <div class="category-title"> Title: ' . $node['title'] . '</div>
        <div class="category-control-icons">
            <div class="control-icon">
                <button class="button-invisible edit-category" data-value="'. $node['id'] .'">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M7.127 22.564l-7.126 1.436 1.438-7.125 5.688 5.689zm-4.274-7.104l5.688 5.689 15.46-15.46-5.689-5.689-15.459 15.46z"/></svg>
                </button>
            </div>
            <div class="control-icon">
                <button class="button-invisible add-sibling" data-value="'. $node['id'] .'">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 26 26">
                        <path fill="#5f6368" stroke="#5f6368"
                              d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 13h-5v5h-2v-5h-5v-2h5v-5h2v5h5v2z"/></svg>
                </button>
            </div>
            <div class="control-icon">
                <form action="/node/delete/'.$node['id']. '" method="post">
                    <input type="text" hidden value="DELETE" name="_method">
                    <input type="text" hidden name="node" value="'. $node['id'] .'">
                    <button type="submit" class="button-invisible">
                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="-1 -1 11 11">
                        <path fill="#5f6368" stroke="#5f6368" stroke-linecap="round" stroke-width="1.5"
                              d="m0 0 9,9 M0 9 9,0" />
                    </svg>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="category-description"> Description: ' . $node['description'] .' </div>
</div>';
            }
        );

        $htmlTree = $repo->childrenHierarchy(
            null, /* starting from root nodes */
            false, /* true: load all children, false: only direct */
            $options
        );

        $this->loadTwigTemplate('cabinet.twig', ['var' => $htmlTree]);
    }

    public function root()
    {
        $input = input()->all();

        $errors = $this->validate($input);

        if(!empty($errors)) response()->redirect('/error');

        $item = new Category();

        $item->setTitle($input['categoryname']);
        $item->setDescription($input['categorydes']);

        $em = $this->getEntityManager();
        try {
            $em->persist($item);
        } catch (ORMException $e) {
        }
        try {
            $em->flush();
        } catch (OptimisticLockException $e) {
        } catch (ORMException $e) {
        }

        response()->redirect('/cabinet');
    }

    public function delete(int $id)
    {
        $em = $this->getEntityManager();
        $repo = $em->getRepository(Category::class);
        $category = $repo->find($id);

        try {
            $em->remove($category);
            $em->flush();
        } catch (ORMException $e) {
        }

        try {
            $em->clear();
        } catch (MappingException $e) {
        }

        response()->redirect('/cabinet');
    }

    public function children(int $id)
    {
        $em = $this->getEntityManager();
        $category = $em->getRepository(Category::class)->find($id);

        $input = input()->all();
        $errors = $this->validate($input);
        if(!empty($errors)) response()->redirect('/error');

        $item = new Category();
        $item->setTitle($input['categoryname']);
        $item->setDescription($input['categorydes']);
        $item->setParent($category);

        try {
            $em->persist($item);
        } catch (ORMException $e) {
        }
        try {
            $em->flush();
        } catch (OptimisticLockException $e) {
        } catch (ORMException $e) {
        }

        response()->redirect('/cabinet');
    }

    public function edit(int $id)
    {;
        $em = $this->getEntityManager();
        $category = $em->getRepository(Category::class)->find($id);

        $input = input()->all();

        if (count($input) === 0) {
            response()->redirect('/cabinet');
        }
        if (!isset($input['categoryname'])) {
            $category->setTitle($input['categoryname']);
        }
        if (!isset($input['categoryname'])) {
            $category->setDescription($input['categorydes']);
        }

        try {
            $em->persist($category);
        } catch (ORMException $e) {
        }

        try {
            $em->flush();
        } catch (OptimisticLockException $e) {
        } catch (ORMException $e) {
        }

        response()->redirect('/cabinet');

    }
}
