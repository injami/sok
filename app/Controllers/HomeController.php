<?php
namespace App\Controllers;

class HomeController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->checkLogin()) return response()->redirect('/cabinet');

        $this->loadTwigTemplate('authorization.twig');
    }

    public function registration()
    {
        $this->loadTwigTemplate('registration.twig');
    }

    public function error()
    {
        $this->loadTwigTemplate('error.twig');
    }

    public function notFound()
    {
        $this->loadTwigTemplate('404.twig');
    }
}
