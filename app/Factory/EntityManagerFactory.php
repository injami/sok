<?php


namespace App\Factory;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\EventManager;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Gedmo\Tree\TreeListener;

class EntityManagerFactory
{
    public function createEntityManager(): EntityManager
    {
        AnnotationRegistry::registerFile(
            __DIR__ .  '/../../vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php'
        );

        $cache = new ArrayCache;
        $annotationReader = new AnnotationReader();
        $cachedAnnotationReader = new CachedReader(
            $annotationReader,
            $cache
        );

        $driverChain = new MappingDriverChain();

        \Gedmo\DoctrineExtensions::registerAbstractMappingIntoDriverChainORM(
            $driverChain,
            $cachedAnnotationReader
        );

        $annotationDriver = new AnnotationDriver(
            $cachedAnnotationReader,
            [__DIR__ . '/../Entity']
        );

        $driverChain->addDriver($annotationDriver,  'App\Entity');

        $devMode = $_ENV['APP_ENV'] === 'development' ? true : false;

        $config = new Configuration();
        $config->setProxyDir(sys_get_temp_dir());
        $config->setProxyNamespace('Proxy');
        $config->setMetadataDriverImpl($driverChain);
        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);

        if ($devMode) {
            $config->setAutoGenerateProxyClasses(false);
        } else {
            $config->setAutoGenerateProxyClasses(true);
        }

        $evm = new EventManager();

        $treeListener = new TreeListener();
        $treeListener->setAnnotationReader($cachedAnnotationReader);
        $evm->addEventSubscriber($treeListener);

        $dbParams = include __DIR__ . '/../../config/db.php';

        try {
            return EntityManager::create($dbParams, $config, $evm);
        } catch (ORMException $e) {
            print_r($e);
            die();
        }
    }
}
