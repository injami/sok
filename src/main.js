import './main.scss'

(function () {

    let buttons = document.querySelectorAll('.add-sibling');

    for (let button of buttons) {
        button.addEventListener('click', event => {
            openForm('sibling', event);
        })
    }

    let editButtons = document.querySelectorAll('.edit-category');

    for (let button of editButtons) {
        button.addEventListener('click', event => {
            openForm('edit', event);
        })
    }

    // TODO getelementby id does not work

    function openForm(type, event) {
        const id = event.currentTarget.dataset.value;
        const action = type === 'sibling' ? `/node/add/children/${id}` : `/node/edit/${id}`;
        const actionType = type === 'sibling' ? 'Add sibling' : 'Edit';
        const html = generateHTML(id, actionType);
        const rootNode = document.getElementById('category' + id);
        const el = document.createElement('FORM');
        el.method = 'POST';
        el.action = action;
        el.innerHTML = html.trim();
        if (rootNode.lastElementChild.tagName === 'FORM') rootNode.lastElementChild.remove();
        rootNode.appendChild(el);
    }

    function generateHTML(id, type) {
        return `<p>${type}</p>
    <label class="text-muted" for="name${id}">Name</label>
    <input type="text" class="form-control" id="name${id}" name="categoryname">
    <label class="text-muted" for="description${id}">Description</label>
    <input type="text" class="form-control" id="description${id}" name="categorydes">
    <button type="submit" class="btn btn-primary add-sibling">Add</button>
    <div class="btn btn-primary" onclick="event.currentTarget.parentNode.remove()">Close</div>`
    }

})();



