<?php


use Pecee\Http\Request;
use Pecee\SimpleRouter\SimpleRouter;
use Pecee\SimpleRouter\Exceptions\NotFoundHttpException;

SimpleRouter::get('/', 'App\Controllers\HomeController@index');

SimpleRouter::get('/cabinet', 'App\Controllers\CategoryController@cabinet');

SimpleRouter::post('/node/add/root', 'App\Controllers\CategoryController@root');

SimpleRouter::post('/node/add/children/{id}', 'App\Controllers\CategoryController@children');

SimpleRouter::delete('/node/delete/{id}', 'App\Controllers\CategoryController@delete');

SimpleRouter::post('/node/edit/{id}', 'App\Controllers\CategoryController@edit');

SimpleRouter::get('/error', 'App\Controllers\HomeController@error');

SimpleRouter::get('/registration', 'App\Controllers\HomeController@registration');

SimpleRouter::get('/logout', 'App\Controllers\UserController@logout');

SimpleRouter::post('/register', 'App\Controllers\UserController@register');

SimpleRouter::post('/authorize', 'App\Controllers\UserController@authorize');

SimpleRouter::get('/notFound', 'App\Controllers\HomeController@notFound');

SimpleRouter::error(function(Request $request, \Exception $exception) {
    if($exception instanceof NotFoundHttpException && $exception->getCode() === 404) {
        response()->redirect('/notFound');
    }
});
