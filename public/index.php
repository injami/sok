<?php

require __DIR__ . '/../vendor/autoload.php';

use Pecee\SimpleRouter\SimpleRouter;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;
use Dotenv\Dotenv as Dotenv;

session_start();

/*Constants*/
define('ROOT', dirname(__DIR__));
define('TEMPLATES', ROOT . '/templates');
define('TEMPLATES_CACHE', TEMPLATES . '/templates_cache');

/* dynamic .env file loader */
$dotenv = Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();
/* Error handler */
$whoops = new Run;
$whoops->pushHandler(new PrettyPageHandler);
$whoops->register();

/* Load external routes file */
require_once ROOT. '/routes/route-helpers.php';
require_once ROOT. '/routes/routes.php';

// Start the routing
SimpleRouter::start();
